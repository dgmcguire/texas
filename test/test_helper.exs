Application.ensure_all_started(:working_on_docs)
Application.ensure_all_started(:hound)
ExUnit.start()
