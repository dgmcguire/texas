defmodule Texas.Controller do
  def texas_render(conn, template, assigns) do
    client_uid = conn.cookies["texas_uuid"]
    init_state = %{view: conn.private[:phoenix_view], data: assigns[:texas]}
    client_pid = {:via, Registry, {CacheRegistry, client_uid}}

    case Texas.Cache.Supervisor.start_client(init_state, client_uid) do
      {:error, :already_started} ->
      GenServer.cast(client_pid, {:update_state, init_state})
      _ -> nil
    end
    Phoenix.Controller.render(conn, template, assigns)
  end

  @doc """
    When given a %Plug.Conn{} redirects regular phoenix redirect function
    otherwise do nothing because it's presumed the developer will broadcast
    out notifications to which properties need to update themselves
  """
  def texas_redirect(%Plug.Conn{} = conn, opts) do
    Phoenix.Controller.redirect(conn, opts)
  end
  def texas_redirect(conn, _opts), do: conn
end
