defmodule Texas.Channel do
  use Phoenix.Channel

  def join("texas:main", %{"cookies" => cookies}, socket) do
    uuid = extract_uuid(cookies)
    socket = assign(socket, :texas_uuid, uuid)
    sync_client(socket)
  end

  def handle_info({:diff, diff}, socket) do
    push(socket, "diff", diff)
    {:noreply, socket}
  end

  def handle_in("main", %{"form_data" => payload}, socket) do
    %{"action" => action, "method" => method} = payload

    verb =
      method
      |> String.downcase()
      |> String.to_atom()

    routes = Application.get_env(:texas, :router).__routes__()
    [route | _] = Enum.filter(routes, &(&1.path == action && &1.verb == verb))
    socket = apply(route.plug, route.opts, [socket, payload])
    GenServer.call(
      {:via, Registry, {CacheRegistry, socket.assigns.texas_uuid}},
      {:update_socket, socket}
    )

    {:reply, :ok, socket}
  end

  def handle_call(:socket, _from, socket) do
    {:reply, socket, socket}
  end

  defp extract_uuid(cookies) do
    cookies = cookie_to_querystring(cookies)
    %{"texas_uuid" => uuid} = URI.decode_query(cookies)
    uuid
  end

  defp cookie_to_querystring(cookies) do
    cookies
    |> String.replace("\s", "&")
    |> String.replace(";", "")
  end

  defp sync_client(%{assigns: %{texas_uuid: uid}} = socket) do
    case Registry.lookup(CacheRegistry, socket.assigns.texas_uuid) do
      [{_pid, []}] ->
        GenServer.call({:via, Registry, {CacheRegistry, uid}}, {:update_socket, socket})
        {:ok, socket}
      _ ->
        {:error, %{"error" => "Cache already exists, refreshing page to sync"}}
    end
  end
end
